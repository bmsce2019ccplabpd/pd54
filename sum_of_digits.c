#include <stdio.h>
int input()
{
    int n;
    printf("Enter a number:");
    scanf("%d",&n);
    return n;
}
int add(int n)
{
   int r,sum=0;
    while(n!=0)
    {
        r=n%10;
        sum=sum+r;
        n=n/10;
    }
    return sum;
}
void output(int sum)
{
    printf("The sum of digits of number =%d\n",sum);
}
int main()
{
    int n,s;
    n=input();
    s=add(n);
    output(s);
}
