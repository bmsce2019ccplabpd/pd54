#include <stdio.h>
int input()
{	
	int a;
	printf("enter a number:");
	scanf("%d",&a);
	return a;
}
void swap(int *a,int *b)
{
	int temp=*a;
	*a=*b;
	*b=temp;
}
void output(int x,int y)
{
	printf("numbers on swapping, a=%d and b=%d\n",x,y);
}
int main()
{
	int r,s;
	r=input();
	s=input();
	swap(&r,&s);
	output(r,s);
	return 0;
}