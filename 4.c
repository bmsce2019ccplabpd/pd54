#include <stdio.h>
int input(int *a,int *b,int *c)
{
    printf("Enter three numbers:");
    scanf("%d %d %d",a,b,c);
}
void largest(int a,int b,int c,int *x,int *y)
{
    if((a>b)&&(a>c))
    {
        *x=a;
        *y=b;
    }
    else if(b>c)
    {
        *x=a;
        *y=c;
    }
    else
    {
        *x=b;
        *y=c;
    }
}
void output(int a,int b,int c,int x,int y)
{
    printf("The largest of two numbers from %d,%d,%d is %d,%d",a,b,c,x,y);
}
int main()
{
    int a,b,c,x,y;
    input(&a,&b,&c);
    largest(a,b,c,&x,&y);
    output(a,b,c,x,y);
}
