#include <stdio.h>
int input()
{
    int n;
    printf("Enter a number");
    scanf("%d",&n);
    return n;
}
int add(int n)
{
    int i,sum=0;
    for(i=0;i<=n;i++)
    {
        if(i%2!=0)
        {
            sum=sum+i;
        }
    }
    return sum;
}
void output(int n,int sum)
{
    printf("The sum of %d odd numbers is %d",n,sum);
}
int main()
{
    int n,a;
    n=input();
    a=add(n);
    output(n,a);
}