#include <stdio.h>
struct time
{
    int h;
    int m;
};
typedef struct time Time;
Time input()
{
    Time t;
    printf("Enter the time (hh,mm):");
    scanf("%d",&t.h,&t.m);
    return t;
}
int min(Time x)
{
    int final,temp;
    temp=x.h*60;
    final=temp+x.m;
    return final;
}
void output(Time y,int z)
{
    printf("The entered time (%d,%d) in  minutes =%d",y.h,y.m,z);
}
int main()
{
        Time T;
        int ans;
        T=input();
        ans=min(T);
        output(T,ans);
        return 0;
}