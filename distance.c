#include<stdio.h>
#include<math.h>
struct point
{
    float x;
    float y;
};
typedef struct point Point;
Point input()
{
    Point a;
    printf("enter a point");
    scanf("%f%f",&a.x,&a.y);
    return a;
    }
float distance(Point P1,Point P2)
{    
    float d;
    d = sqrt((P1.x-P2.x)*(P1.x-P2.x)+(P1.y-P2.y)*(P1.y-P2.y));
    return d;
}
void output(Point a,Point b,float d)
{
    printf("The distance between (%f,%f) and (%f,%f) is %f",a.x,a.y,b.x,b.y,d);
}
    int main()
{
    float dist;
    Point P1,P2;
    P1= input();
    P2= input();
    dist= distance(P1,P2);
    output(P1,P2,dist);
    return 0; 
}